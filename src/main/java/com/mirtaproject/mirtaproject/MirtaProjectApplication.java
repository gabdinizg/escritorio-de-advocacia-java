package com.mirtaproject.mirtaproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MirtaProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MirtaProjectApplication.class, args);
	}

}
