function reveal() {
  var reveals = document.querySelectorAll(".reveal");

  for (var i = 0; i < reveals.length; i++) {
    var windowHeight = window.innerHeight;
    var elementTop = reveals[i].getBoundingClientRect().top;
    var elementVisible = 150;

    if (elementTop < windowHeight - elementVisible) {
      reveals[i].classList.add("active");
    } else {
      reveals[i].classList.remove("active");
    }
  }
}

$(function() {
  $(".icone-pergunta").click(function(evento) {
    const divPergunta = $(evento.target.parentElement.parentElement).find(".resposta");
    $(evento.target).toggleClass("icone-pergunta-rotation")

    if(divPergunta.hasClass("inativo")){
      divPergunta.removeClass("inativo");
      divPergunta.addClass("ativo");
    } else {
      divPergunta.removeClass("ativo");
      divPergunta.addClass("inativo");
    }
  });
});

window.addEventListener("scroll", reveal);